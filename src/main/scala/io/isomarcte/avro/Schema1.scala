package io.isomarcte.avro

import org.apache.avro._
import org.apache.avro.generic._
import scala.util.Failure
import scala.util.Try

object Schema1 {
  sealed trait Body extends Product with Serializable {
    def toGenericDataRecord(schema: Schema): Try[GenericData.Record]
  }

  object Body {
    final case class Foo(bar: String) extends Body {
      override final def toGenericDataRecord(schema: Schema): Try[GenericData.Record] =
        SchemaHelper.getUnionSchema(schema)("Foo").fold(
          Failure(new IllegalArgumentException("Invalid Schema")): Try[GenericData.Record]
        )((s: Schema) =>
          Try(
            new GenericRecordBuilder(s).set(
              s.getField("bar"), this.bar
            ).build
          )
        )
    }
    final case class Baz(baz: Int) extends Body {
      override final def toGenericDataRecord(schema: Schema): Try[GenericData.Record] =
        SchemaHelper.getUnionSchema(schema)("Baz").fold(
          Failure(new IllegalArgumentException("Invalid Schema")): Try[GenericData.Record]
        )((s: Schema) =>
          Try(
            new GenericRecordBuilder(s).set(
              s.getField("baz"), this.baz
            ).build
          )
        )
    }
  }

  final case class Payload(someString: String, body: Body) {
    def toGenericDataRecord(schema: Schema): Try[GenericData.Record] =
      for {
        bodyField <- Try(schema.getField("body"))
        bodyRecord <- this.body.toGenericDataRecord(bodyField.schema())
        someStringField <- Try(schema.getField("someString"))
        result <- Try(new GenericRecordBuilder(schema).set(someStringField, this.someString).set(bodyField, bodyRecord).build)
      } yield result
    }
}
