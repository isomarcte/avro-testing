package io.isomarcte.avro

import scala.util.Try
import java.io.InputStream
import org.apache.avro.compiler.idl.Idl
import org.apache.avro.Protocol

object Parser {
  def parseProtocol(is: InputStream): Try[Protocol] =
    Try(new Idl(is).CompilationUnit())
}
