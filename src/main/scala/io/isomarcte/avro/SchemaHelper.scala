package io.isomarcte.avro

import org.apache.avro._
import scala.jdk.CollectionConverters._

object SchemaHelper {

  def getUnionSchema(schema: Schema)(name: String): Option[Schema] =
    if (schema.isUnion) {
      schema.getTypes.asScala.toList.find(_.getName == name)
    } else {
      None
    }
}
