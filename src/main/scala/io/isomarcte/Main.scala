package io.isomarcte

import java.io.InputStream
import java.io.ByteArrayOutputStream
import scala.util.Try
import io.isomarcte.avro._
import org.apache.avro.generic._
import org.apache.avro.file._
import org.apache.avro.io._
import org.apache.avro._
import scala.jdk.CollectionConverters._

object Main {
  val schema1CP: String =
    "io/isomarcte/avro/schema1.avdl"
  val schema2CP: String =
    "io/isomarcte/avro/schema2.avdl"

  val schema1Payload: Schema1.Payload =
    Schema1.Payload(
      "schema1",
      Schema1.Body.Foo("bar")
    )

  val encoderFactory: EncoderFactory =
    new EncoderFactory()

  lazy val classLoader: ClassLoader =
    this.getClass().getClassLoader()

  def schemaAsStream(value: String): Try[InputStream] =
    Try(
      this.classLoader.getResourceAsStream(value)
    )

  def writeRecord(s: Option[Schema])(value: Vector[GenericData.Record]): Try[Array[Byte]] = Try {
    val baos: ByteArrayOutputStream = new ByteArrayOutputStream()
    val writer: DatumWriter[GenericData.Record] =
      new GenericDatumWriter()
    val dfw: DataFileWriter[GenericData.Record] =
      new DataFileWriter(writer)
    val schema: Schema = s.getOrElse(value.head.getSchema())

    dfw.create(schema, baos)
    value.foreach(dfw.append(_))
    dfw.flush()
    dfw.close()

    baos.toByteArray()
  }

  def readRecord(schema: Option[Schema])(bytes: Array[Byte]): Try[Vector[GenericData.Record]] = Try {
    val input: SeekableInput =
      new SeekableByteArrayInput(bytes)
    val reader: DatumReader[GenericData.Record] =
      schema.fold(new GenericDatumReader(): DatumReader[GenericData.Record])(s => new GenericDatumReader(s))
    val dfr: FileReader[GenericData.Record] =
      DataFileReader.openReader(input, reader)
    dfr.iterator().asScala.toVector
  }

  def main(args: Array[String]): Unit = {
    val protocol1: Try[Protocol] =
      this.schemaAsStream(this.schema1CP).flatMap(Parser.parseProtocol)
    val protocol2: Try[Protocol] =
      this.schemaAsStream(this.schema2CP).flatMap(Parser.parseProtocol)
    val schema1: Try[Schema] = protocol1.flatMap(p => Try(p.getType("Payload")))
    val schema2: Try[Schema] = protocol2.flatMap(p => Try(p.getType("Payload")))

    // Make a record from a Protocol1.Payload
    val result: Try[GenericData.Record] =
      schema1.flatMap(s => schema1Payload.toGenericDataRecord(s))

    // Write it out as bytes (including the Schema as required by Avro)
    val bytes: Try[Array[Byte]] = result.flatMap(a => this.writeRecord(None)(Vector(a)))

    // Print that as a String
    bytes.foreach(a => println(new String(a)))

    // Read it back as a Protocol1
    val read0: Try[Vector[GenericData.Record]] = for {
      s1 <- schema1
      s2 <- schema2
      b <- bytes
      result1 <- this.readRecord(Some(s1))(b)

      // Should work even without given it a schema, as the schema is in the bytes.
      result2 <- this.readRecord(None)(b)

      result3 <- this.readRecord(Some(s2))(b)
    } yield result1 ++ result2 ++ result3
    read0.foreach(_.foreach(println))

    // Write them all back as both Schema 1 and 2

    val bytes1: Try[Array[Byte]] = for {
      records <- read0
      s1 <- schema1
      results <- this.writeRecord(Some(s1))(records)
    } yield results

    val bytes2: Try[Array[Byte]] = for {
      records <- read0
      s2 <- schema2
      results <- this.writeRecord(Some(s2))(records)
    } yield results

    bytes1.foreach(a => println(new String(a)))
    bytes2.foreach(a => println(new String(a)))
  }
}
