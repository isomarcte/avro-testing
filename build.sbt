// Constants //

lazy val projectName = "avro-testing"
lazy val scala213 = "2.13.1"

// Group //

lazy val apacheAvroG = "org.apache.avro"

// Artifact //

lazy val avroA         = "avro"
lazy val avroCompilerA = "avro-compiler"

// Version //

lazy val avroV       = "1.9.2"

// GAVs //

lazy val avro         = apacheAvroG % avroA         % avroV
lazy val avroCompiler = apacheAvroG % avroCompilerA % avroV

// Root Project //

lazy val root = (project in file(".")).settings(
  name := s"${projectName}-root",
  libraryDependencies ++= Seq(
    avro,
    avroCompiler
  ),
  inThisBuild(
    Seq(
      scalaVersion := scala213
    )
  )
)
